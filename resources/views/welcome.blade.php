<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>proxy test</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                @if(isset($response))
                    <div class="row h-100">
                        <div class="col-sm-12 my-auto">
                            @if(isset($response['code'])&&$response['code']===0)
                                <h3>The request failed.</h3>
                            @elseif(isset($response['code']))
                                <h3>{{$response['code']}} @if(isset(config('status_codes')[$response['code']])) {{config('status_codes')[$response['code']]}} @endif</h3>
                                <h3>{{$response['time']}} ms</h3>
                            @elseif(isset($response['error']))
                                <h3>{{$response['error']}}</h3>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-8 order-md-1">
                <div class="py-5">
                    <h2>Proxy Tester</h2>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="/test" class="needs-validation" novalidate>
                            @csrf
                            <div class="mb-3">
                                <div class="input-group">
                                    <input type="text"
                                           class="form-control @error('url') is-invalid @enderror"
                                           value="@if(old('url')!==null) {{old('url')}} @elseif(isset($data['url'])) {{$data['url']}} @endif"
                                           name="url" id="url" placeholder="Enter the URL" required="">
                                    <div class="invalid-feedback">
                                        Invalid URL.
                                    </div>
                                </div>
                            </div>
                            <div class="py-5">
                                <h3>Proxy Options</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-10 mb-5">
                                    <input type="text"
                                           class="form-control @error('ip') is-invalid @enderror"
                                           value="@if(old('ip')!==null) {{old('ip')}} @elseif(isset($data['ip'])) {{$data['ip']}} @endif"
                                           name="ip" id="ip" placeholder="Proxy IP Adress" required="">
                                    <div class="invalid-feedback">
                                        Invalid IP.
                                    </div>
                                </div>
                                <div class="col-md-2 mb-1">
                                    <input type="text"
                                           class="form-control @error('port') is-invalid @enderror"
                                           value="@if(old('port')!==null) {{old('port')}} @elseif(isset($data['port'])) {{$data['port']}} @endif"
                                           name="port" id="port" placeholder="Port" required="">
                                    <div class="invalid-feedback">
                                        Invalid port.
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <input type="text"
                                           class="form-control" @error('username') is-invalid @enderror
                                           value="@if(old('username')!==null) {{old('username')}} @elseif(isset($data['username'])) {{$data['username']}} @endif"
                                           name="username" id="username" placeholder="Username" required="">
                                    <div class="invalid-feedback">
                                        Invalid username.
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input type="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password" id="password" placeholder="Password" required="">
                                    <div class="invalid-feedback">
                                        Invalid password.
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9 mb-4"></div>
                                <div class="col-md-3 mb-4">
                                    <button class="btn btn-primary btn-lg btn-block" type="submit">Start test</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
