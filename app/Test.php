<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Test extends Model
{
    protected $table = 'tests';
    protected $fillable = [
        'url',
        'ip',
        'port',
        'username',
        'password',
        'code',
        'time',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Crypt::encryptString($value); //crypt the password
    }
}
