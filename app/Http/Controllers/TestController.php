<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {
        $response = [];
        //validation
        $request->validate([
            'url'       => 'required|active_url',
            'ip'        => 'required|ip',
            'port'      => 'required|numeric|min:1|max:65535',
            'password'  => 'required_with:username',
        ]);
        $data = $request->all();
        $proxy = $data['ip'].':'.$data['port'];
        if($request->has('username')){
            $auth = $data['username'].':'.$data['password'];
        }
        $handle = curl_init(); //init curl
        curl_setopt($handle, CURLOPT_URL, $data['url']);//set curl url
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_PROXY, $proxy); //set curl proxy
        curl_setopt($handle, CURLOPT_TIMEOUT, 50); //set timeout in seconds
        if(isset($auth)){
            curl_setopt($handle, CURLOPT_PROXYUSERPWD, $auth); //set proxy auth if present
        }
        if (curl_errno($handle)) {
            $response['error'] = curl_error($handle); //get error if any
        }
        curl_exec($handle);
        $response['code'] = curl_getinfo($handle, CURLINFO_HTTP_CODE); //get response header code
        $response['time'] = round((curl_getinfo($handle, CURLINFO_TOTAL_TIME)*1000),2); //get request time in milliseconds
        Test::create([
            'url'       => $data['url'],
            'ip'        => $data['ip'],
            'port'      => $data['port'],
            'username'  => $data['username'],
            'password'  => $data['password'],
            'code'      => $response['code'],
            'time'      => $response['time'],
        ]); //save data in the db
        return view('welcome',['data'=>$data,'response'=>$response]);
    }
}
